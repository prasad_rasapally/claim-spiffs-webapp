import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SpiffApiGatewayServiceSdkConfig} from 'spiff-api-gateway-service-sdk';
import {ClaimSpiffServiceSdkConfig} from 'claim-spiff-service-sdk';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {SessionManagerConfig} from 'session-manager';
import {PartnerRepAssociationServiceSdkConfig} from 'partner-rep-association-service-sdk';
import {AccountPermissionsServiceSdkConfig} from 'account-permissions-service-sdk';

export default class Config {

    _identityServiceSdkConfig:IdentityServiceSdkConfig;

    _spiffApiGatewayServiceSdkConfig:SpiffApiGatewayServiceSdkConfig;

    _claimSpiffServiceSdkConfig:ClaimSpiffServiceSdkConfig;

    _partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig;

    _partnerRepAssociationServiceSdkConfig:PartnerRepAssociationServiceSdkConfig;

    _accountPermissionsServiceSdkConfig:AccountPermissionsServiceSdkConfig;

    _sessionManagerConfig:SessionManagerConfig;


    /**
     * @param {IdentityServiceSdkConfig} identityServiceSdkConfig
     * @param {SpiffApiGatewayServiceSdkConfig} spiffApiGatewayServiceSdkConfig
     * @param {ClaimSpiffServiceSdkConfig} claimSpiffServiceSdkConfig
     * @param {PartnerRepServiceSdkConfig} partnerRepServiceSdkConfig
     * @param {PartnerRepAssociationServiceSdkConfig} partnerRepAssociationServiceSdkConfig
     * @param {AccountPermissionsServiceSdkConfig} accountPermissionsServiceSdkConfig
     * @param {SessionManagerConfig} sessionManagerConfig
     */
    constructor(identityServiceSdkConfig:IdentityServiceSdkConfig,
                spiffApiGatewayServiceSdkConfig:SpiffApiGatewayServiceSdkConfig,
                claimSpiffServiceSdkConfig:ClaimSpiffServiceSdkConfig,
                partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig,
                partnerRepAssociationServiceSdkConfig:PartnerRepAssociationServiceSdkConfig,
                accountPermissionsServiceSdkConfig:AccountPermissionsServiceSdkConfig,
                sessionManagerConfig:SessionManagerConfig) {

        if (!identityServiceSdkConfig) {
            throw new TypeError('identityServiceSdkConfig required');
        }
        this._identityServiceSdkConfig = identityServiceSdkConfig;

        if (!spiffApiGatewayServiceSdkConfig) {
            throw new TypeError('spiffApiGatewayServiceSdkConfig required');
        }
        this._spiffApiGatewayServiceSdkConfig = spiffApiGatewayServiceSdkConfig;

        if (!claimSpiffServiceSdkConfig) {
            throw new TypeError('claimSpiffServiceSdkConfig required');
        }
        this._claimSpiffServiceSdkConfig = claimSpiffServiceSdkConfig;

        if (!partnerRepServiceSdkConfig) {
            throw new TypeError('partnerRepServiceSdkConfig required');
        }
        this._partnerRepServiceSdkConfig = partnerRepServiceSdkConfig;

        if (!partnerRepAssociationServiceSdkConfig) {
            throw new TypeError('partnerRepAssociationServiceSdkConfig required');
        }
        this._partnerRepAssociationServiceSdkConfig = partnerRepAssociationServiceSdkConfig;

        if(!accountPermissionsServiceSdkConfig) {
            throw new TypeError('accountPermissionsServiceSdkConfig required');
        }
        this._accountPermissionsServiceSdkConfig = accountPermissionsServiceSdkConfig;

        if (!sessionManagerConfig) {
            throw new TypeError('sessionManagerConfig');
        }
        this._sessionManagerConfig = sessionManagerConfig;

    }

    get identityServiceSdkConfig() {
        return this._identityServiceSdkConfig;
    }

    get spiffApiGatewayServiceSdkConfig() {
        return this._spiffApiGatewayServiceSdkConfig;
    }

    get claimSpiffServiceSdkConfig() {
        return this._claimSpiffServiceSdkConfig;
    }

    get partnerRepServiceSdkConfig() {
        return this._partnerRepServiceSdkConfig;
    }

    get partnerRepAssociationServiceSdkConfig() {
        return this._partnerRepAssociationServiceSdkConfig;
    }

    get accountPermissionsServiceSdkConfig() {
        return this._accountPermissionsServiceSdkConfig;
    }

    get sessionManagerConfig() {
        return this._sessionManagerConfig;
    }
}