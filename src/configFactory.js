import Config from './config';
import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SpiffApiGatewayServiceSdkConfig} from 'spiff-api-gateway-service-sdk';
import {ClaimSpiffServiceSdkConfig} from 'claim-spiff-service-sdk';
import {SessionManagerConfig} from 'session-manager';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {PartnerRepAssociationServiceSdkConfig} from 'partner-rep-association-service-sdk';
import {AccountPermissionsServiceSdkConfig} from 'account-permissions-service-sdk';

export default class ConfigFactory {

    /**
     * @param {object} data
     * @returns {Config}
     */
    static construct(data):Config {

        const identityServiceSdkConfig =
            new IdentityServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const spiffApiGatewayServiceSdkConfig =
            new SpiffApiGatewayServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const claimSpiffServiceSdkConfig =
            new ClaimSpiffServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const partnerRepServiceSdkConfig=
            new PartnerRepServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const partnerRepAssociationServiceSdkConfig =
            new PartnerRepAssociationServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const accountPermissionsServiceSdkConfig =
            new AccountPermissionsServiceSdkConfig(
              data.precorConnectApiBaseUrl
            );

        const sessionManagerConfig =
            new SessionManagerConfig(
                data.precorConnectApiBaseUrl,
                data.sessionManagerConfig.loginUrl,
                data.sessionManagerConfig.logoutUrl
            );

        return new Config(
            identityServiceSdkConfig,
            spiffApiGatewayServiceSdkConfig,
            claimSpiffServiceSdkConfig,
            partnerRepServiceSdkConfig,
            partnerRepAssociationServiceSdkConfig,
            accountPermissionsServiceSdkConfig,
            sessionManagerConfig
        );

    }

}
