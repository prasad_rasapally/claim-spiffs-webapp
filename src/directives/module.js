import angular from 'angular';
import directive from './accountPermissionDirective';

export default angular
    .module('accountPermission.module', [])
    .directive(directive.name, directive);



