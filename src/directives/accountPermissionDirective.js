import template from './accountPermission.html!text';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'
import AccountPermissionController from './controller'

function accountPermission($uibModal) {
    return {
        replace: false,
        controller: AccountPermissionController,
        controllerAs: 'controller',
        bindToController: true,
        link : function (scope,element,attributes) {
            scope.countryFlag()
                .then((result)=>{
                    element.show();
                    scope.isAuthorized = true;
                })
                .catch(error=>{
                    element.hide();
                    scope.isAuthorized = false;
                    $uibModal.open({
                        scope: scope,
                        template: template,
                        size: 'sm',
                        backdrop: 'static'
                    });
                });
        }
    }
}

export default accountPermission;