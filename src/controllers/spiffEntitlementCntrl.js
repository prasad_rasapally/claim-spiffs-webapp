import IdentityServiceSdk from 'identity-service-sdk';
import SpiffApiGatewayServiceSdk from 'spiff-api-gateway-service-sdk';
import {UploadPartnerSaleInvoiceReqWebDto} from 'spiff-api-gateway-service-sdk';
import {SpiffEntitlementWithPartnerRepInfoWebView} from 'spiff-api-gateway-service-sdk';
import {PartnerRepInfoWebView} from 'spiff-api-gateway-service-sdk';
import ClaimSpiffServiceSdk from 'claim-spiff-service-sdk';
import PartnerRepAssociationServiceSdk from 'partner-rep-association-service-sdk';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';
import SessionManager from 'session-manager';

import promptOnEmptySelection from './templates/promptOnEmptySelection.html!text';

/**
 * TODO: Try to extract this to separate generic filter file.
 */
angular
    .module('spiffEntitlement',['ui.bootstrap','mgcrea.ngStrap','mgcrea.ngStrap.modal'])
    .filter('startFrom', function() {
        return function(input, start) {
            if(input) {
                start = +start; //parse to int
                return input.slice(start);
            }
            return [];
        }
    });

export default class SpiffEntitlementCntrl {

    _partnerReps;

    _spiffEntitlements;

    _spiffApiGatewayServiceSdk:SpiffApiGatewayServiceSdk;

    _claimSpiffServiceSdk:ClaimSpiffServiceSdk;

    _partnerRepAssociationServiceSdk:PartnerRepAssociationServiceSdk;

    _partnerRepService:PartnerRepServiceSdk;

    _identityServiceSdk:IdentityServiceSdk;

    _sessionManager:SessionManager;

    _$q;

    constructor($q,
                $scope,
                Upload,
                config,
                $modal,
                $uibModal,
                filterFilter,
                $location,
                spiffApiGatewayServiceSdk:SpiffApiGatewayServiceSdk,
                claimSpiffServiceSdk:ClaimSpiffServiceSdk,
                partnerRepAssociationServiceSdk:PartnerRepAssociationServiceSdk,
                partnerRepServiceSdk,
                identityServiceSdk:IdentityServiceSdk,
                sessionManager:SessionManager,
                $timeout
                ) {
        $scope.loader = false;
        if (!$q) {
            throw new TypeError('$q required');
        }
        this._$q = $q;
        
        if (!$uibModal) {
            throw new TypeError('$uibModal required');
        }
        this._$uibModal = $uibModal;

        if (!$scope) {
            throw new TypeError('$scope required');
        }
        this._$scope = $scope;

        if (!spiffApiGatewayServiceSdk) {
            throw new TypeError('spiffApiGatewayServiceSdk required');
        }
        this._spiffApiGatewayServiceSdk = spiffApiGatewayServiceSdk;

        if (!claimSpiffServiceSdk) {
            throw new TypeError('claimSpiffServiceSdk required');
        }
        this._claimSpiffServiceSdk = claimSpiffServiceSdk;

        if (!partnerRepAssociationServiceSdk) {
            throw new TypeError('partnerRepAssociationServiceSdk required');
        }
        this._partnerRepAssociationServiceSdk = partnerRepAssociationServiceSdk;

        if (!partnerRepServiceSdk) {
            throw new TypeError('partnerRepServiceSdk required');
        }
        this._partnerRepServiceSdk = partnerRepServiceSdk;

        if (!identityServiceSdk) {
            throw new TypeError('identityServiceSdk required');
        }
        this._identityServiceSdk = identityServiceSdk;

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        $scope.getSpiff = function () {
            $scope.loader = true;
            $scope.getSpiffDealers = {};
            $q(resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    )
            ).then(accessToken => {
                $scope.loader = true;
                identityServiceSdk
                    .getUserInfo(accessToken)
                    .then(userInfo => {

                        var promise1 =
                            $q((resolve,reject)=>{
                                spiffApiGatewayServiceSdk
                                    .listPartnerRepsInfoWithAccountId(
                                        userInfo._account_id,
                                        accessToken)
                                    .then(result =>resolve(result))
                                    .catch(error => {
                                        $scope.iserror=true;
                                        $scope.errorMessage="Error in loading DealerRep Dropdown";
                                        $("#err").show().fadeOut(3000);
                                        $scope.loader = false;
                                        $scope.$apply();
                                        console.log("partnerReps error :", error);
                                        reject(error)
                                    });
                            });

                        var promise2 =
                            $q((resolve,reject)=>{
                                spiffApiGatewayServiceSdk
                                    .listSpiffEntitlementsAccountId(
                                        userInfo._account_id,
                                        accessToken)
                                    .then(result=>resolve(result))
                                    .catch(error => {
                                        $scope.iserror=true;
                                        $scope.errorMessage="Error in loading Entitlement Grid";
                                        $("#err").show().fadeOut(3000);
                                        $scope.loader = false;
                                        $scope.$apply();
                                        console.log("entitlements error :", error);
                                        reject(error)
                                    }
                                    );
                            });

                       Promise.all(
                           [
                           promise1,
                           promise2
                           ]
                       )
                       .then(results => {
                           $scope.loader = false;
                           $scope.dealers = results[0];
                           $scope.getSpiffDealers = results[1];

                           $scope.tempUsers = $scope.getSpiffDealers;

                           $scope.currentPage = 1; //current page
                           $scope.maxSize = 5; //pagination max size
                           $scope.entryLimit = 7; //max rows for data table
                                // init pagination with $scope.list
                           $scope.noOfPages = Math.ceil($scope.getSpiffDealers.length/$scope.entryLimit);
                           $scope.filtered=$scope.tempUsers;

                           $scope.select_change=function(){
                                $scope.$watch('search', function(term) {
                                $scope.filtered = filterFilter($scope.getSpiffDealers, term);
                                $scope.tempUsers=$scope.filtered;
                                if($scope.getSpiffDealers) $scope.noOfPages = Math.ceil($scope.filtered.length/$scope.entryLimit);
                                    });
                                };
                           $scope.$apply();

                        });
                    })
                }
            );
        };

        $scope.query = '';
        $scope.search = function (dealerSearch) {
            var query = $scope.query;
            var id = dealerSearch.partnerRepUserId;
            if(query == ""){
                return true;
            }else  if (query===id) {
                return true;
            }else{
                return false;
            }

        };


        $scope.checkList = [];

        $scope.CheckedDealer = function (CheckedDetails) {

            var json = {};
            json['spiffEntitlementId'] = CheckedDetails.spiffEntitlementId;
            json['partnerSaleRegistrationId'] = CheckedDetails.partnerSaleRegistrationId;
            json['partnerRepUserId'] = CheckedDetails.partnerRepUserId;
            json['installDate'] = CheckedDetails.installDate;
            json['spiffAmount'] = CheckedDetails.spiffAmount;
            json['facilityName'] = CheckedDetails.facilityName;
            json['invoiceNumber'] = CheckedDetails.invoiceNumber;
            json['invoiceUrl'] = CheckedDetails.invoiceUrl;
            json['sellDate'] = CheckedDetails.sellDate;
            CheckedDetails.isChecked = true;

            var flag = false;
            angular.forEach($scope.checkList, function (k, i) {
                console.log("selected before push",$scope.checkList);
                if (CheckedDetails.spiffEntitlementId == k.spiffEntitlementId) {
                    var index = $scope.checkList.indexOf(k);
                    CheckedDetails.isChecked = false;
                    $scope.checkList.splice(index, 1);
                    flag = true;
                }
            });
            if (flag == false)
                $scope.checkList.push(json);
            console.log("selected after push",$scope.checkList);
        };

        $scope.setCSS = function (isBnkInfoExists, isW9InfoExists, isConcatInfoExists, index) {

            var missingString = "";
            if (!isBnkInfoExists) {
                missingString = "bank info";
            }

            if (!isW9InfoExists) {
                if (missingString != "") missingString += " and ";
                missingString += "w9 info";
            }

            if (!isConcatInfoExists) {
                if (missingString != "") missingString += " and ";
                missingString += "contact info";
            }

            missingString += " missed";

            if (!isBnkInfoExists || !isW9InfoExists || !isConcatInfoExists) {
                var selObj = ".selectpicker > option:eq(" + (index + 1) + ")";
                $(selObj).data("icon", "glyphicon glyphicon-warning-sign text-color");
                $(selObj).data("subtext", "(" + missingString + ")");
                return "";
            }
        };

        $scope.setMissingInfo = function (isBnkInfoExists, isW9InfoExists, isConcatInfoExists, index) {

            var missingString = "";

            if (!isBnkInfoExists) {
                missingString = "bank info";
            }

            if (!isW9InfoExists) {
                if (missingString != "") missingString += " and ";
                missingString += "w9 info";
            }

            if (!isConcatInfoExists) {
                if (missingString != "") missingString += " and ";
                missingString += "contact info";
            }

            missingString += " missed";

            if (!isBnkInfoExists || !isW9InfoExists || !isConcatInfoExists) {
                var selObj = ".innerSelectpicker > option:eq(" + (index + 1) + ")";
                $(selObj).data("icon", "glyphicon glyphicon-warning-sign text-color");
                $(selObj).data("subtext", "(" + missingString + ")");
                return "";
            }
        };


        $scope.setSPIFF = function (isBnkInfoExists, isW9InfoExists, isConcatInfoExists) {
            if (!isBnkInfoExists || !isW9InfoExists || !isConcatInfoExists) {
                return "glyphicon glyphicon-warning-sign";
            }
        };

        $scope.setS = function (isBnkInfoExists, isW9InfoExists, isConcatInfoExists, isInvoiceUrl) {
            if (isBnkInfoExists && isW9InfoExists && isConcatInfoExists && isInvoiceUrl) {
                $scope.isMissingInfo = false;
                $scope.clicked = false;
            } else {
                $scope.isMissingInfo = true;
                $scope.clicked = true;
            }
            if (!isInvoiceUrl) {
                return "glyphicon glyphicon-upload";
            }
        };

        $scope.open_modal = function (data) {
            var myOtherModal = $modal({
                scope: $scope,
                title: "Edit Registration",
                contentTemplate: "templates/editRegistration.html"
            });

            if(!data.firstName){

                $q(resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(accessToken =>
                            resolve(accessToken)
                            )
                )
                .then(accessToken => {
                    $scope.loader = true;
                    $q(resolve =>
                        identityServiceSdk
                            .getUserInfo(accessToken)
                            .then(userInfo =>
                                resolve(userInfo)
                            )
                    )
                    .then(userInfo => {
                        $q(resolve =>
                            spiffApiGatewayServiceSdk
                                .listActivePartnerRepsInfoWithAccountId(
                                    userInfo._account_id,
                                    accessToken
                                )
                                .then(partnerReps => {
                                        resolve(partnerReps)
                                    }
                                ).catch(function (e) {
                                    console.log("error in partnerReps......", e);
                                    $scope.loader = false;
                                })
                        )
                        .then(partnerReps => {
                            $scope.loader = false;
                            $scope.activePartnerReps = partnerReps;
                        })
                    })
                });
            }
            $scope.selectedRegistration = data;
        };

        $scope.handleFileSelection = function($files) {
            $scope.invoiceFileToUpload = $files ? $files[0]: null;
        };

        $scope.invoice_submit= function(){
            $scope.loader = true;
            $q(resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    )
            ).then(accessToken => {
                $scope.loader = true;
                var partnerRepUserId = $scope.selectedRegistration_dealer_dropdown_selected_rep_id;
                var partnerSaleRegistrationId = $scope.selectedRegistration.partnerSaleRegistrationId;
                var partnerSaleInvoiceNumber = $scope.selectedRegistration.invoiceNumber;
                var baseUrl = config.spiffApiGatewayServiceSdkConfig.precorConnectApiBaseUrl;

                if($scope.invoiceFileToUpload) {
                    /**
                     * TODO: Try to move this to javascript SDK.
                     */
                    Upload.upload({
                        url: baseUrl + '/spiff-api-gateway/uploadinvoice',
                        headers: {
                            'Authorization': `Bearer ${accessToken}`
                        },
                        data: {partnerSaleInvoiceNumber:partnerSaleInvoiceNumber, partnerSaleRegId:partnerSaleRegistrationId, file: $scope.invoiceFileToUpload}
                    }).then(response => {
                        this.$hide();
                        $scope.loader=false;
                        $modal({scope: $scope,title:"Success",content:"Invoice uploaded successfully"});
                        console.log("upload succeeded:", response);
                        $scope.invoiceFileToUpload = null;
                        $scope.getSpiff();
                    },function (error) {
                        $scope.iserror = true;
                        $scope.errorMessage="Error in Uploading Invoice";
                        $("#err").show().fadeOut(3000);
                        $scope.loader=false;
                        //$scope.$apply();
                        console.log("Invoice upload error :", error);
                    }
                    );
                }
                else{
                    $scope.loader = false;
                    $scope.errorMessage = "Please choose file to upload";
                    $("#err").show().fadeOut(4000);
                }
            });
        };

        $scope.update_dealer_submit = function() {
            $scope.loader = true;

            $q(resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    )
            ).then(accessToken => {
                $scope.loader = true;
                var partnerRepUserId = $scope.selectedRegistration_dealer_dropdown_selected_rep_id;
                var partnerSaleRegistrationId = $scope.selectedRegistration.partnerSaleRegistrationId;
                if (typeof(partnerRepUserId) !== "undefined") {
                    $scope.selectedRegistration_dealer_dropdown_selected_rep_id = undefined;
                    claimSpiffServiceSdk.updatePartnerRep(partnerSaleRegistrationId, partnerRepUserId, accessToken);
                    this.$hide();
                    $scope.loader = false;
                    $scope.getSpiff();
                    $modal({scope: $scope,title:"Success",content:"DealerRep is updated successfully"});
                }
                else {
                    $scope.loader = false;
                    $scope.errorMessage = "Please select a Dealer Rep";
                    $("#err").show().fadeOut(4000);
                }
            });
        };

        $scope.claimSpiffEntitlements = function () {
            //console.log('$scope.checkList',$scope.checkList);
            //window.localStorage.setItem("checkedList", JSON.stringify($scope.checkList));
            if ($scope.checkList.length === 0) {
                /*$modal({
                    scope: $scope,
                    title: "Select Records",
                    content: "Please check atleast one record to proceed."
                });*/
                this.modalInstance = this.controller._$uibModal.open({
                    animation : true,
                    scope : $scope,
                    template : promptOnEmptySelection,
                    size : 'md'
                });

                $scope.closeEmptyRequestPrompt = function(){
                    this.modalInstance.dismiss('cancel');
                }
            }
            else {
                $scope.loader = true;
                $q(resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(accessToken =>
                            resolve(accessToken)
                        )
                ).then(accessToken => {
                    $scope.loader = true;
                    identityServiceSdk
                        .getUserInfo(accessToken)
                        .then(userInfo => {
                            spiffApiGatewayServiceSdk
                                .claimSpiffEntitlements(userInfo._account_id, JSON.stringify($scope.checkList), accessToken)
                                .then(response => {
                                    window.localStorage.setItem("submittedClaims",JSON.stringify(response));
                                    $location.path("/spiffClaims");
                                    $scope.$apply();
                                    $scope.loader=false;
                                }).catch(error => {
                                console.log("error inside claim spiffs:", error);
                            });
                        });
                    }
                );

            }
        };

        $scope.cancel_submit = function () {
            $scope.getSpiff();
            $scope.query = '';
            $scope.checkList = [];
        };

        $scope.changeCount = function (id) {

           $scope.selectedRegistration_dealer_dropdown_selected_rep_id = id;

        };


    }
}

SpiffEntitlementCntrl
    .$inject = [
        '$q',
        '$scope',
        'Upload',
        'config',
        '$modal',
        '$uibModal',
        'filterFilter',
        '$location',
        'spiffApiGatewayServiceSdk',
        'claimSpiffServiceSdk',
        'partnerRepAssociationServiceSdk',
        'partnerRepServiceSdk',
        'identityServiceSdk',
        'sessionManager',
        '$timeout'
    ];
